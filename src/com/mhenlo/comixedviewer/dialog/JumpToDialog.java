package com.mhenlo.comixedviewer.dialog;

import android.app.Dialog;
import android.content.Context;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.mhenlo.comixedviewer.ComixedViewer;
import com.mhenlo.comixedviewer.R;

public class JumpToDialog extends Dialog {
	
	private ComixedViewer mComixedViewer;
	
	private EditText mEditTextNumber;
	private TextView mTextNumbers;
	private Button mButtonJump;
	private Button mButtonCancel;
	
	
	public JumpToDialog(Context pContext) {
		super(pContext);
		mComixedViewer = (ComixedViewer) pContext;
	}

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		Log.d("DBug","JumpToDialog.onCreate()");
		super.onCreate(savedInstanceState);
		Log.d("DBug","JumpToDialog.java -> SuperConsturctor erfolgreich aufgerufen");
		
		try {
			setContentView(R.layout.jumptodialog); // GUI definieren
		} catch (Exception e) {
			Log.d("DBug","JumpToDialog.java -> Exception:\n" +e);
		}
		Log.d("DBug","JumpToDialog.java -> ContentView ist gesettet");
		this.setTitle(R.string.jumpToDialog_title);
		getWindow().setLayout(WindowManager.LayoutParams.FILL_PARENT, WindowManager.LayoutParams.WRAP_CONTENT);
		Log.d("DBug","JumpToDialog.java -> Fensterlayout gesetzt");
		
		mEditTextNumber = (EditText) findViewById(R.id.jumpToDialog_editText);
		mTextNumbers = (TextView) findViewById(R.id.jumpToDialog_textNumbers);
		mButtonJump = (Button) findViewById(R.id.jumpToDialog_buttonOk);
		mButtonCancel = (Button) findViewById(R.id.jumpToDialog_buttonCancel);
		
		Log.d("DBug","JumpToDialog.java -> Views geladen");
		
		mButtonJump.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				cancel();
				mComixedViewer.showPicture( Integer.parseInt(mEditTextNumber.getText().toString()) - 1 );
			}
		});
		mButtonCancel.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				cancel();
			}
		});
		
		Log.d("DBug","JumpToDialog.java -> OnClickListeners gesetzt");
	}

	@Override
	protected void onStart() {
		super.onStart();
		mEditTextNumber.setText( "" +(ComixedViewer.getCurrentPictureNo() + 1 ) );
		mTextNumbers.setText( mComixedViewer.getString(R.string.jumpToDialog_addNumber) +" 1 " +mComixedViewer.getString(R.string.jumpToDialog_to) +" " +mComixedViewer.getPictureCount() +" " +mComixedViewer.getString(R.string.jumpToDialog_inclusive) );
		
	}
	
	
	
}
