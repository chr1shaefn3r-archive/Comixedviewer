package com.mhenlo.comixedviewer.dialog;

import android.app.Dialog;
import android.content.Context;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.TextView;

import com.mhenlo.comixedviewer.ComixedViewer;
import com.mhenlo.comixedviewer.R;
import com.mhenlo.comixedviewer.filemanagement.FileInformation;
import com.mhenlo.comixedviewer.filemanagement.FileManager;

public class FileInformationDialog extends Dialog {
	
	@SuppressWarnings("unused")
	private Context mContext;
	private Button mButtonBack;
	
	private TextView mTextViewName;
	private TextView mTextViewPath;
	private TextView mTextViewId;
	private TextView mTextViewDimension;
	private TextView mTextViewFilesize;
	
	public FileInformationDialog(Context pContext) {
		super(pContext);
		this.mContext = pContext;
	}

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		Log.d("DBug", "FileInformationDialog.onCreate()");
		super.onCreate(savedInstanceState);
		setContentView(R.layout.fileinformationdialog); // GUI definieren
		this.setTitle(R.string.fileInformationDialog_title);
		
		getWindow().setLayout(WindowManager.LayoutParams.FILL_PARENT, WindowManager.LayoutParams.WRAP_CONTENT);
		
		mButtonBack = (Button) findViewById(R.id.fileInformationDialogButtonBack);
		mButtonBack.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				cancel();
			}
		});
		
		mTextViewName = (TextView) findViewById(R.id.fileInformationDialogName);
		mTextViewPath = (TextView) findViewById(R.id.fileInformationDialogPath);
		mTextViewId = (TextView) findViewById(R.id.fileInformationDialogId);
		mTextViewDimension = (TextView) findViewById(R.id.fileInformationDialogDimension);
		mTextViewFilesize = (TextView) findViewById(R.id.fileInformationDialogFilesize);
		
		Log.d("DBug", "FileInformationDialog created");
	}
	
	protected void onStart() {
		Log.d("DBug", "FileInformationDialog onStart()");
		
		FileInformation fileInfo = FileManager.getFileInformation(ComixedViewer.getCurrentPictureNo());
		mTextViewName.setText("" +fileInfo.getName());
		mTextViewPath.setText("" +fileInfo.getPath());
		mTextViewId.setText("" +( Integer.parseInt(fileInfo.getId()) + 1 ) );
		mTextViewDimension.setText("" +fileInfo.getDimension());
		mTextViewFilesize.setText("" +fileInfo.getSize());
		
		Log.d("DBug", "FileInformationDialog started");
	}

}
