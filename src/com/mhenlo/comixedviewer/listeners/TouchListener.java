package com.mhenlo.comixedviewer.listeners;

import android.view.GestureDetector;
import android.view.MotionEvent;
import android.view.View;

import com.mhenlo.comixedviewer.ComixedViewer;

public class TouchListener implements View.OnTouchListener {

	private ComixedViewer mComixedViewer;
	private GestureDetector mGestureDetector;

	private float mXDown;
	private float mXUp;
	private float mYDown;
	private float mYUp;
	private int mVisibilityDown;
	
	
	public TouchListener(ComixedViewer pComixedViewer, GestureDetector pGestureDetector) {
		mComixedViewer = pComixedViewer;
		mGestureDetector = pGestureDetector;
	}

	@Override
	public boolean onTouch(View v, MotionEvent event) {
		
		if ( mGestureDetector.onTouchEvent(event) ) {
			return true;
		}
		if ( event.getAction() == MotionEvent.ACTION_MOVE ) {
			mComixedViewer.setZoomButtonsVisibility(View.INVISIBLE);
		}
		if ( event.getAction() == MotionEvent.ACTION_DOWN ) {
			mXDown = event.getX();
			mYDown = event.getY();
			mVisibilityDown = mComixedViewer.getZoomButtonsVisibility();
		}
		if ( event.getAction() == MotionEvent.ACTION_UP ) {
			mXUp = event.getX();
			mYUp = event.getY();
			
			if ( Math.abs( mXDown - mXUp ) < 10 &&
					Math.abs( mYDown - mYUp ) < 10 ) {
				if ( mComixedViewer.getZoomButtonsVisibility() == View.INVISIBLE && mVisibilityDown == View.INVISIBLE ) {
					mComixedViewer.setZoomButtonsVisibility(View.VISIBLE);
				} else {
					mComixedViewer.setZoomButtonsVisibility(View.INVISIBLE);
				}
			}
		}
		
		return false;
	}

}
