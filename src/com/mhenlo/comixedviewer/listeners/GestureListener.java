package com.mhenlo.comixedviewer.listeners;

import android.util.Log;
import android.view.GestureDetector.SimpleOnGestureListener;
import android.view.MotionEvent;

import com.mhenlo.comixedviewer.ComixedViewer;

public class GestureListener extends SimpleOnGestureListener {
	
	private int mSwipeMinDistance;
	private int mSwipeMaxOffPath;
	private int mSwipeVelocity;
	
	private ComixedViewer mComixedViewer;
	
	
	public GestureListener(int pScreenWidth, int pScreenHeight, ComixedViewer pComixedViewer) {
		mSwipeMinDistance = pScreenWidth / 3;
		mSwipeMaxOffPath = pScreenHeight / 6;
		mSwipeVelocity = pScreenWidth / 6;
		
		mComixedViewer = pComixedViewer;
	}
	
	
	@Override
	public boolean onFling(MotionEvent pMotionEvent1, MotionEvent pMotionEvent2, float pVelocityX, float pVelocityY) {
		Log.d("DBug","GestureListener.onFling()");
		
		if (Math.abs(pMotionEvent1.getY() - pMotionEvent2.getY()) > mSwipeMaxOffPath) { // Ueberpruefung auf vertikale Abweichung
			return false;
		} else if(pMotionEvent1.getX() - pMotionEvent2.getX() > mSwipeMinDistance && Math.abs(pVelocityX) > mSwipeVelocity) { // Swipe von rechts nach links
			mComixedViewer.changePicture(+1);
			return true;
		} else if (pMotionEvent2.getX() - pMotionEvent1.getX() > mSwipeMinDistance && Math.abs(pVelocityX) > mSwipeVelocity) { // Swipe von links nach rechts
			mComixedViewer.changePicture(-1);
			return true;
		}
		return false;
	} 
	
}
