package com.mhenlo.comixedviewer;

import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;
import android.util.Log;

public class Prefs{
	
	private SharedPreferences mSharedprefs = null;
	private SharedPreferences.Editor mEditor = null;
	
	private static final String FOLDERPATH = "preferencesFolderPath";
	private static final String PICTURE_ID = "preferencesLastPicture";
	private static final String PICTURE_COUNT = "preferencesPictureCount";
	
	public Prefs(Context pContext) {
		Log.d("DBug","Prefs.java -> Prefs()");
		mSharedprefs = PreferenceManager.getDefaultSharedPreferences(pContext);
		mEditor = mSharedprefs.edit();
	}
	
	
	public String getPath() {
		return mSharedprefs.getString(FOLDERPATH, "");
	}
	public void setPath(String pPath) {
		mEditor.putString(FOLDERPATH, pPath);
		mEditor.commit();
	}
	
	public int getPictureId() {
		return mSharedprefs.getInt(PICTURE_ID, 0);
	}
	public void setPictureId(int pPictureId) {
		mEditor.putInt(PICTURE_ID, pPictureId);
		mEditor.commit();
	}
	
	public int getPictureCount() {
		return mSharedprefs.getInt(PICTURE_COUNT, 0);
	}
	public void setPictureCount(int pPictureCount) {
		mEditor.putInt(PICTURE_COUNT, pPictureCount);
		mEditor.commit();
	}

}
