package com.mhenlo.comixedviewer.provider;

import java.util.HashMap;

import android.app.SearchManager;
import android.net.Uri;
import android.provider.BaseColumns;

public class SearchSuggestions implements BaseColumns {
	
	public static final Uri CONTENT_URI = Uri.parse("content://" + SearchSuggestionsContentProvider.AUTHORITY + "/searchsuggestions");
	public static final String CONTENT_TYPE = "vnd.mhenlo.cursor.dir/vnd.mhenlo.searchsuggestions";
	
	public static final String TABLE = "searchsuggestions";
	
	public static final String KEY_WORD = SearchManager.SUGGEST_COLUMN_TEXT_1;
	public static final String COLUMN_QUERY = SearchManager.SUGGEST_COLUMN_QUERY;
	
	public static HashMap<String, String> mSearchSuggestionsProjectionMap;
	static {
		mSearchSuggestionsProjectionMap = new HashMap<String, String>();
		mSearchSuggestionsProjectionMap.put(SearchSuggestions.KEY_WORD, SearchSuggestions.KEY_WORD);
		mSearchSuggestionsProjectionMap.put(SearchSuggestions.COLUMN_QUERY, SearchSuggestions.COLUMN_QUERY);
		mSearchSuggestionsProjectionMap.put(BaseColumns._ID, "rowid AS " + BaseColumns._ID);
		mSearchSuggestionsProjectionMap.put(SearchManager.SUGGEST_COLUMN_INTENT_DATA_ID, "rowid AS " + SearchManager.SUGGEST_COLUMN_INTENT_DATA_ID);
	}
	
}
