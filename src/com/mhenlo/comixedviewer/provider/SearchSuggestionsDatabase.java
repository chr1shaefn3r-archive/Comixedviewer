package com.mhenlo.comixedviewer.provider;

import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteException;
import android.database.sqlite.SQLiteOpenHelper;
import android.database.sqlite.SQLiteQueryBuilder;
import android.database.sqlite.SQLiteStatement;
import android.util.Log;

public class SearchSuggestionsDatabase {
	
	private final SearchSuggestionsDatabaseHelper mSearchSuggestionsDatabaseHelper;
	
	public SearchSuggestionsDatabase(Context pContext) {
		mSearchSuggestionsDatabaseHelper = new SearchSuggestionsDatabaseHelper(pContext);
		
	}
	
	public void wipeData() {
		mSearchSuggestionsDatabaseHelper.onUpgrade(mSearchSuggestionsDatabaseHelper.getWritableDatabase(), 0, 0);
	}
	
	public void addSuggestions(String[] pFileNames) {
		SQLiteDatabase db = mSearchSuggestionsDatabaseHelper.getWritableDatabase();
		String insertSqlStatement = "insert into "+SearchSuggestions.TABLE+" ("+SearchSuggestions.KEY_WORD+", "+SearchSuggestions.COLUMN_QUERY+") values (?, ?)";
		SQLiteStatement insert = db.compileStatement(insertSqlStatement);
        try {
        	db.beginTransaction();
            for(String fileName : pFileNames) {
            	insert.bindString(1, fileName);
            	insert.bindString(2, fileName);
            	insert.executeInsert();
            }
            db.setTransactionSuccessful();
        } catch (SQLiteException e) {
        	Log.e("DBug", this.getClass() +"SQLiteException: " + e.getLocalizedMessage());
        } finally {
        	db.endTransaction();
        	insert.close();
        }
        return;
    }
	
	public Cursor query(String[] pProjection, String pSelection, String[] pSelectionArgs, String pSortOrder) {
		SQLiteQueryBuilder qb = new SQLiteQueryBuilder();
		qb.setTables(SearchSuggestions.TABLE);
		qb.setProjectionMap(SearchSuggestions.mSearchSuggestionsProjectionMap);
		
		SQLiteDatabase db = mSearchSuggestionsDatabaseHelper.getReadableDatabase();
		
		Cursor c = qb.query(db, pProjection, pSelection, pSelectionArgs, null, null, pSortOrder);
		return c;
	}
	
	public void close() {
		mSearchSuggestionsDatabaseHelper.close();
	}
	
	private static class SearchSuggestionsDatabaseHelper extends SQLiteOpenHelper {

		private static final String DATABASE_NAME = "searchsuggestions.db";
		private static int DATABASE_VERSION = 1;
		
		public SearchSuggestionsDatabaseHelper(Context pContext) {
			super(pContext, DATABASE_NAME, null, DATABASE_VERSION);
		}

		@Override
		public void onCreate(SQLiteDatabase pDb) {
			pDb.execSQL("CREATE VIRTUAL TABLE " + SearchSuggestions.TABLE +
					" USING fts3 (" +
					SearchSuggestions.KEY_WORD + ", " +
					SearchSuggestions.COLUMN_QUERY + ");");
		}

		@Override
		public void onUpgrade(SQLiteDatabase pDb, int pOldVersion, int pNewVersion) {
			pDb.execSQL("DROP TABLE IF EXISTS "+SearchSuggestions.TABLE);
	        onCreate(pDb);
		}
	}	
}
