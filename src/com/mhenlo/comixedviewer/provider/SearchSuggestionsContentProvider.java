package com.mhenlo.comixedviewer.provider;

import android.app.SearchManager;
import android.content.ContentProvider;
import android.content.ContentValues;
import android.content.UriMatcher;
import android.database.Cursor;
import android.net.Uri;
import android.util.Log;

public class SearchSuggestionsContentProvider extends ContentProvider {

	public static final String AUTHORITY = "com.mhenlo.comixedviewer.provider.SearchSuggestionsContentProvider";
	
    private SearchSuggestionsDatabase mSearchSuggestionsDBHelper;
	
    // UriMatcher stuff
	private static final int SEARCH_SUGGEST = 1;
	private static final UriMatcher mUriMatcher = buildUriMatcher();
	
	private static UriMatcher buildUriMatcher() {
		UriMatcher matcher = new UriMatcher(UriMatcher.NO_MATCH);
		matcher.addURI(AUTHORITY, SearchManager.SUGGEST_URI_PATH_QUERY, SEARCH_SUGGEST);
		matcher.addURI(AUTHORITY, SearchManager.SUGGEST_URI_PATH_QUERY+"/*", SEARCH_SUGGEST);
		return matcher;
	}
    
    @Override
	public boolean onCreate() {
		mSearchSuggestionsDBHelper = new SearchSuggestionsDatabase(getContext());
		return true;
	}
    
    @Override
	public Cursor query(Uri pUri, String[] pProjection, String pSelection, String[] pSelectionArgs, String pSortOrder) {
		switch (mUriMatcher.match(pUri)) {
		case SEARCH_SUGGEST:
			if (pSelectionArgs == null) {
				throw new IllegalArgumentException("pSelectionArgs must be prvoided for the Uri: " + pUri);
			}
			Log.d("DBug", "Kilroy was here! with query: "+pSelectionArgs[0].toLowerCase());
			Cursor cursor = mSearchSuggestionsDBHelper.query(null, SearchSuggestions.KEY_WORD + " MATCH ?", new String[] {pSelectionArgs[0].toLowerCase()+"*"}, null);
			String[] columns = cursor.getColumnNames();
			for(int i=0; columns.length < i; i++) {
				Log.i("Debug", i+". Spalte: "+columns[i]);
			}
			while(cursor.moveToNext()) {
				for(int i=0; columns.length < i; i++) {
					Log.i("Debug", columns[i]+": "+cursor.getString(cursor.getColumnIndex(columns[i])));
				}
			}
			return cursor;
		default:
			throw new IllegalArgumentException("Unknown URI " + pUri);
		}
	}

    @Override
	public String getType(Uri pUri) {
		switch (mUriMatcher.match(pUri)) {
		case SEARCH_SUGGEST:
			return SearchManager.SUGGEST_MIME_TYPE;
		default:
			throw new IllegalArgumentException("Unknown URI " + pUri);
		}
	}
    
	@Override
	public Uri insert(Uri pUri, ContentValues pContentValues) {
		throw new UnsupportedOperationException();
	}
	
	@Override
	public int delete(Uri pUri, String pSelection, String[] pSelectionArgs) {
		throw new UnsupportedOperationException();
	}

	@Override
	public int update(Uri pUri, ContentValues pContentValues, String pSelection, String[] pSelectionArgs) {
		throw new UnsupportedOperationException();
	}
}
