package com.mhenlo.comixedviewer.filemanagement;

import java.io.File;
import java.io.FilenameFilter;

public class PictureFilter implements FilenameFilter {

	@Override
	public boolean accept(File pDir, String pFilename) {
		if ( pFilename.toLowerCase().endsWith(".jpg") ) {
			return true;
		}
		if ( pFilename.toLowerCase().endsWith(".jpeg") ) {
			return true;
		}
		if ( pFilename.toLowerCase().endsWith(".png") ) {
			return true;
		}
		return false;
	}

}
