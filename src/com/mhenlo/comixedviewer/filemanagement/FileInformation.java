package com.mhenlo.comixedviewer.filemanagement;

public class FileInformation {
	
	private String mName;
	private String mPath;
	private String mId;
	private String mDimension;
	private String mSize;
	
	public FileInformation(String pName, String pPath, String pId, String pDimension, String pSize) {
		mName = pName;
		mPath = pPath;
		mId = pId;
		mDimension = pDimension;
		mSize = pSize;
	}

	public String getName() {
		return mName;
	}

	public String getPath() {
		return mPath;
	}

	public String getId() {
		return mId;
	}

	public String getDimension() {
		return mDimension;
	}

	public String getSize() {
		return mSize;
	}
	
}
