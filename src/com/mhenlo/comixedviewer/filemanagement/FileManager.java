package com.mhenlo.comixedviewer.filemanagement;

import java.io.File;

import com.mhenlo.comixedviewer.ComixedViewer;

public class FileManager {
	
	public static FileInformation getFileInformation(int pPictureId) {
		
		File file = new File( ComixedViewer.getCurrentPicturePath(pPictureId) );
		
		String name = file.getName().substring( 0, file.getName().lastIndexOf(".") );
		String path = file.getAbsolutePath();
		String dimension = "" +ComixedViewer.getCurrentPicture().getWidth() +" x " +ComixedViewer.getCurrentPicture().getHeight();
		long size = file.length();
		String sizeString = "" +size +" Byte";
		
		if ( size > 1073741824 ) {
			sizeString = "" +( size / 1073741824 ) +" GiB (" +( size / 1000000000 ) +" GB)";
		} else if ( size > 1048576 ) {
			sizeString = "" +( size / 1048576 ) +" MiB (" +( size / 1000000 ) +" MB)";
		} else if ( size > 1024 ) {
			sizeString = "" +( size / 1024 ) +" KiB (" +( size / 1000 ) +" KB)";
		}
		
		FileInformation fileInfo = new FileInformation(name, path, "" +pPictureId, dimension, sizeString);
		
		return fileInfo;
	}
	
}
