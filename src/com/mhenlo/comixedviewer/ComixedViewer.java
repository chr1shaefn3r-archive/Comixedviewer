package com.mhenlo.comixedviewer;

import java.io.File;
import java.io.FilenameFilter;
import java.util.Comparator;

import android.app.Activity;
import android.app.SearchManager;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.os.Bundle;
import android.os.Environment;
import android.util.Log;
import android.view.GestureDetector;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.ScrollView;
import android.widget.Toast;

import com.mhenlo.comixedviewer.dialog.FileInformationDialog;
import com.mhenlo.comixedviewer.dialog.JumpToDialog;
import com.mhenlo.comixedviewer.filemanagement.PictureFilter;
import com.mhenlo.comixedviewer.listeners.GestureListener;
import com.mhenlo.comixedviewer.listeners.TouchListener;
import com.mhenlo.comixedviewer.provider.SearchSuggestionsDatabase;

public class ComixedViewer extends Activity {
	
	private static final String PATH = "mPath";
	private static final String PICTURESTRINGS = "sPictures";
	private static final String PICTURENUMBER = "sCurrentPictureNo";
	private static final String ZOOMFAKTOR = "sZoomFactor";

	private static boolean pathIsOK = false;
	private String mPath;
	private static String sEntirePath;
	private static String[] sPictures;
	private static int sPictureCount = 0;
	private static int sCurrentPictureNo = 0;
	private static double sZoomFactor = 1.0;
	
	
	private Prefs mPrefs;
	private FilenameFilter mFileFilter = new PictureFilter();
	
	// Werte des HTC-Desire; werden in der onResume() ordentlich initialisiert
	private static int sScreenWidth = 480;
	private static int sScreenHeight = 800;
	
	private GestureDetector mGestureDetector;
	private TouchListener mTouchListener;
	private ScrollView mScrollView;
	private ImageView mImageView;
	private static Bitmap sBitmap;
	private ImageButton mButtonZoomOut;
	private ImageButton mButtonZoomIn;
	
	private FileInformationDialog mFileInformationDialog;
	private JumpToDialog mJumpToDialog;
	
	private SearchSuggestionsDatabase mSearchSuggestionsDatabase;
	
	@Override
	public void onCreate(Bundle savedInstanceState) {
		Log.d("DBug", this.getClass() +": onCreate()");
		super.onCreate(savedInstanceState);
		setContentView(R.layout.comixedviewer); // GUI definieren
		Log.d("DBug", this.getClass() +": setContentView() erfolgreich");
		
		mSearchSuggestionsDatabase = new SearchSuggestionsDatabase(this);
		
		initializeUI(); // Variablen der GUI initialisieren
		
		mFileInformationDialog = new FileInformationDialog(this);
		mJumpToDialog = new JumpToDialog(this);
		mPrefs = new Prefs(this);
		
		Log.d("DBug", this.getClass() +": onCreate() - Bundle: " +savedInstanceState);
		
		if ( savedInstanceState != null ) {
			
			Log.d("DBug", "onCreate() -> Aus dem Bundle:");
			pathIsOK = true;
			mPath = savedInstanceState.getString(PATH);
			Log.d("DBug", "Pfad: " +mPath);
			sPictureCount = savedInstanceState.getStringArray(PICTURESTRINGS).length;
			Log.d("DBug", "sPictureCount: " +sPictureCount);
			sPictures = savedInstanceState.getStringArray(PICTURESTRINGS);
			for ( int i = 0; i < sPictureCount; ++i ) {
				Log.d("DBug", "Datei " +i +": " +sPictures[i]);
			}
			
			sCurrentPictureNo = savedInstanceState.getInt(PICTURENUMBER);
			Log.d("DBug", "sCurrentPictureNo: " +sCurrentPictureNo);
			
			sZoomFactor = savedInstanceState.getDouble(ZOOMFAKTOR);
			Log.d("DBug", "sZoomFactor: " +sZoomFactor);
			
			setSearchSuggestions();
		}
		
	}
	
	private void initializeUI() { // GUI-Elemente "finden" oder erzeugen und Einstellungen vornehmen
		mGestureDetector = new GestureDetector( new GestureListener(sScreenWidth, sScreenHeight, this) );
		mTouchListener = new TouchListener(this, mGestureDetector);
		
		mScrollView = (ScrollView)findViewById(R.id.scrollView);
		mScrollView.setBackgroundColor(Color.BLACK);
		mScrollView.setOnTouchListener(mTouchListener);
		
		mImageView = (ImageView) findViewById(R.id.imageView);
		mImageView.setAdjustViewBounds(true);
		mImageView.setBackgroundColor(Color.BLACK);
		mImageView.setPadding(0, 10, 0, 10);
		
		mButtonZoomOut = (ImageButton) findViewById(R.id.buttonZoomMinus);
		mButtonZoomOut.setEnabled(false);
		mButtonZoomOut.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				zoomOut();
			}
		});
		mButtonZoomOut.setVisibility(View.INVISIBLE);
		
		mButtonZoomIn = (ImageButton) findViewById(R.id.buttonZoomPlus);
		mButtonZoomIn.setEnabled(false);
		mButtonZoomIn.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				zoomIn();
			}
		});
		mButtonZoomIn.setVisibility(View.INVISIBLE);
	}
	
	@Override
	protected void onResume() {
		super.onResume();
		sScreenWidth = getWindowManager().getDefaultDisplay().getWidth();
		sScreenHeight = getWindowManager().getDefaultDisplay().getHeight();
		
		Log.d("DBug", "OnResume() --> Pfad ist ok: " +pathIsOK);
		
		if ( mPath == null ) {
			checkPath();
		} else if ( mPrefs.getPath().compareTo(mPath) != 0 ) {
			checkPath();
		}
		
		changePicture(0); // das aktuelle Bild anzeigen
	}
	
	@Override
	protected void onPause() {
		super.onPause();
		mPrefs.setPictureId(sCurrentPictureNo);
	}
	
	@Override
	protected void onDestroy() {
		if ( mSearchSuggestionsDatabase != null ) {
			mSearchSuggestionsDatabase.close(); // Datenbank zur Speicherfreigabe schliessen
		}
		super.onDestroy();
	}
	
	@Override
	protected void onSaveInstanceState(Bundle pBundle) {
		Log.d("DBug", this.getClass() +": onSaveInstanceState()");
		if ( pathIsOK ) {
			pBundle.putString(PATH, mPath);
			pBundle.putStringArray(PICTURESTRINGS, sPictures);
			for ( int i = 0; i < sPictures.length; ++i ) {
				Log.d("DBug", this.getClass() +": Sring has been put into saveBundle: " +sPictures[i]);
			}
			pBundle.putInt(PICTURENUMBER, sCurrentPictureNo);
			pBundle.putDouble(ZOOMFAKTOR, sZoomFactor);
		}
		
		super.onSaveInstanceState(pBundle);
	}
	
	
	public void checkPath() { // ueberprueft den in den Preferences angegebenen Pfad und liest enthaltene Bilder aus
		mButtonZoomOut.setEnabled(false);
		mButtonZoomIn.setEnabled(false);
		
		Log.d("DBug", "CheckPath() aufgerufen!");
		
		if( !Environment.getExternalStorageState().equals(Environment.MEDIA_MOUNTED) ) { // Falls SD-Karte NICHT gemountet ist
			Log.d("DBug", "KEINE SD-KARTE GEMOUNTET!");
			Toast.makeText( this, "" +this.getString(R.string.error_noSd), Toast.LENGTH_SHORT ).show();
			pathIsOK = false;
			return;
		}
		
		Log.d("DBug", "Vormals gespeicherter Pfad: " +mPath );
		Log.d("DBug", "Pfad aus den Preferences:   " +mPrefs.getPath() );
		Log.d("DBug", "Pfad war beim letzten Check OK: " +pathIsOK );
		if ( mPath != null ) {
			if ( pathIsOK && mPath.compareTo(mPrefs.getPath()) == 0 ) { // Falls der Pfad schon gecheckt wurde
				Log.d("DBug", "Pfad ist wurde schon gecheckt! Zeige Bild...");
				return;
			}
		}
		pathIsOK = false;
		sCurrentPictureNo = 0;
		sPictureCount = 0;
		
		
		Log.d("DBug", "Pfad wird gecheckt!");
		mPath = mPrefs.getPath(); // Holt den Pfad aus den Einstellungen
		
		if ( mPath.compareTo("") == 0 ) { // Falls der Pfad leer ist
			Log.d("DBug", "PFAD IST LEER!");
			Toast.makeText( this, this.getString(R.string.error_noPath), Toast.LENGTH_SHORT ).show();
			return;
		}
		
		Log.d("DBug", "Path uebertragen:" +mPath);
		while ( mPath.startsWith("\\") || mPath.startsWith("/") ) { // Schleife zur Ueberpruefung, ob der Pfad-String mit einem "/" oder "\" beginnt
			mPath = mPath.substring(1);
			Log.d("DBug", "Slash davor entfernt");
		}
		while ( mPath.endsWith("\\") || mPath.endsWith("/") ) { // Schleife zur Ueberpruefung, ob der Pfad-String mit einem "/" oder "\" endet
			mPath = mPath.substring( 0 , mPath.length() - 1 );
			Log.d("DBug", "Slash danach entfernt");
		}
		mPrefs.setPath(mPath); // Korrigierten Pfad direkt in Preferences speichern
		
		Log.d("DBug", "Path vor Zusammenfuehrung: " +mPath);
		sEntirePath = "" +Environment.getExternalStorageDirectory().getAbsolutePath() +"/" +mPath; // Fuegt den SD-Karten - Praefix hinzu
		Log.d("DBug", "Path nach Zusammenfuehrung: " +sEntirePath);
		
		
		File folder = new File( sEntirePath ); // File-Objekt aus angegebenem Pfad erzeugen
		if ( !folder.exists() ) { // Falls kein Ordner mit diesem Pfad existiert
			Log.d("DBug", "ORDNER EXISTIERT NICHT");
			Toast.makeText( this, "\"" +folder.getAbsolutePath() +"\" " +this.getString(R.string.error_pathInvalid), Toast.LENGTH_SHORT ).show();
			return;
		}
		
		sPictureCount = folder.listFiles( mFileFilter ).length;
		Log.d("DBug", "Anzahl Bilder im Ordner: " +sPictureCount); // Anzahl der Bilder
		
		if ( sPictureCount < 1 ) { // Falls es keine Bilder gibt
			Log.d("DBug", "KEINE BILDER IM ORDNER");
			Toast.makeText( this, this.getString(R.string.error_noPictures), Toast.LENGTH_SHORT ).show();
			return;
		}
		
		Log.d("DBug", " --> Bilder werden geladen..." );
		Toast.makeText( this, this.getString(R.string.output_loading) +" " +sPictureCount +" " +this.getString(R.string.output_pictures) , Toast.LENGTH_SHORT ).show();
		
		
		// TODO: Ladeanimation waehrend dem Laden der Bilder
		
		sPictures = new String[sPictureCount]; // File[] erzeugen
		
		sPictures = folder.list(mFileFilter); // passende Bilder in File[] uebertragen
		
//		Log.d("DBug sort", "Vor Sortierung:");
//		for ( int i = 0; i < sPictures.length; ++i ) {
//			Log.d("DBug sort", sPictures[i].getName());
//		}
		java.util.Arrays.sort(sPictures, new Comparator<String>() {
			@Override
			public int compare(String pString1, String pString2) {
				return pString1.toLowerCase().compareTo(pString2.toLowerCase());
			}
		});
//		Log.d("DBug sort", "Nach Sortierung:");
//		for ( int i = 0; i < sPictures.length; ++i ) {
//			Log.d("DBug sort", sPictures[i].getName());
//		}
		
		Toast.makeText( this, "" +sPictureCount +" " +this.getString(R.string.output_success) , Toast.LENGTH_SHORT ).show();
		Log.d("DBug", "Alle Bilder geladen!");
		
		pathIsOK = true;
		
		if ( mPath.compareTo(mPrefs.getPath()) == 0 && sPictureCount == mPrefs.getPictureCount() ) {
			sCurrentPictureNo = mPrefs.getPictureId();
		}
		
		sZoomFactor = 1.0;
		
		setSearchSuggestions();
		
		Log.d("DBug", "Pfad wurde gecheckt und ist ok.");
	}
	
	public static String getEntirePath() {
		return sEntirePath;
	}
	
	public void changePicture(int pDirection) {
		if ( !pathIsOK )
			return;
		
		Log.d("DBug", "ComixedViewer.changePicture -> [relativ]: " +pDirection);
		// Aendert die Nummer des aktuellen Bildes unter Beachtung der Anzahl der Bilder
		int step = 0;
		if (pDirection < 0) {
			step = -1;
		} else if ( pDirection > 0 ) {
			step = 1;
		}
		Log.d("DBug","ComixedViewer.changePicture() -> von " +sCurrentPictureNo +" + " +step +"...");
		int newPictureNumber = sCurrentPictureNo;
		
		for ( int i = 0; i < Math.abs(pDirection); ++i ) {
			if ( (newPictureNumber + step) < 0 ) {
				newPictureNumber = sPictureCount - 1;
			}	
			else if ( (newPictureNumber + step) >= sPictureCount ) {
				newPictureNumber = 0;
			} else {
				newPictureNumber += step;
			}
		}
		Log.d("DBug","ComixedViewer.changePicture() -> ...zu " +newPictureNumber);
		showPicture(newPictureNumber);
	}
	
	public void showPicture(int pPictureNumber) { // Aendert die Nummer des aktuellen Bildes unter Beachtung der Anzahl der Bilder
		if ( !pathIsOK )
			return;
		
		if ( sZoomFactor == 1.0 ) {
			mButtonZoomOut.setEnabled(true);
			mButtonZoomIn.setEnabled(false);
		} else if ( sZoomFactor == 0.5 ) {
			mButtonZoomOut.setEnabled(false);
			mButtonZoomIn.setEnabled(true);
		} else {
			mButtonZoomOut.setEnabled(true);
			mButtonZoomIn.setEnabled(true);
		}
		
		if ( sCurrentPictureNo != pPictureNumber ) {
			if ( pPictureNumber < 0 ) {
				sCurrentPictureNo = 0;
			} else if (pPictureNumber >= sPictureCount) {
				sCurrentPictureNo = sPictureCount - 1;
			} else {
				sCurrentPictureNo = pPictureNumber;
			}
			mScrollView.scrollTo(0, 0); // Nach oben scrollen
		}
		Log.d("DBug", "ComixedViewer.showPicture() -> Aktuelles Bild: " +sCurrentPictureNo +" (" +sEntirePath +"/" +sPictures[sCurrentPictureNo] +")");
		
		if ( sBitmap != null )
			sBitmap.recycle();
		sBitmap = BitmapFactory.decodeFile( sEntirePath +"/" +sPictures[sCurrentPictureNo] ); // Bitmap aus aktuellem Bild erzeugen...
		
		mImageView.destroyDrawingCache();
		mImageView.setImageBitmap( Bitmap.createScaledBitmap( // ...und dem ImageView zuweisen.
				sBitmap,
				(int) ( sScreenWidth * sZoomFactor ),
				(int) ( ( ( sScreenWidth *sZoomFactor )  * sBitmap.getHeight() ) / sBitmap.getWidth() ),
				true) );
		mImageView.destroyDrawingCache();
		sBitmap.recycle();
	}
	
	public int getZoomButtonsVisibility() {
		return mButtonZoomIn.getVisibility();
	}
	
	public void setZoomButtonsVisibility(int pVisibility) {
		mButtonZoomOut.setVisibility(pVisibility);
		mButtonZoomIn.setVisibility(pVisibility);
	}
	
	public static int getCurrentPictureNo() {
		return sCurrentPictureNo;
	}
	
	public static String getCurrentPicturePath(int pPictureId) {
		return sEntirePath +"/" +sPictures[pPictureId];
	}
	
	public static Bitmap getCurrentPicture() {
		return sBitmap;
	}
	
	public int getPictureCount() {
		return sPictureCount;
	}
	
	private void zoomOut() {
		mButtonZoomIn.setEnabled(true);
		sZoomFactor = ( sZoomFactor * 9.0 ) / 10.0;
		if ( sZoomFactor < 0.5 ) {
			sZoomFactor = 0.5;
			mButtonZoomOut.setEnabled(false);
		}
		changePicture(0);
	}
	
	private void zoomIn() {
		mButtonZoomOut.setEnabled(true);
		sZoomFactor = ( sZoomFactor * 10.0 ) / 9.0;
		if ( sZoomFactor > 1.0 ) {
			sZoomFactor = 1.0;
			mButtonZoomIn.setEnabled(false);
		}	
		changePicture(0);
	}
	
	/*************************************************************************/
	/* Search-Methoden                                                       */
	/*************************************************************************/
	@Override
	public boolean onSearchRequested() {
		if ( sPictureCount > 0 ) {
			startSearch("", false, null, false);
			return true;
		} else {
			return false;
		}
	}
	
	private void setSearchSuggestions() {
		Log.d("DBug", this.getClass() +": Adding Suggestions...");
		mSearchSuggestionsDatabase.wipeData();
		mSearchSuggestionsDatabase.addSuggestions(sPictures);
		Log.d("DBug", this.getClass() +": Suggestions added!");
	}
	
	@Override
	protected void onNewIntent(Intent pIntent) {
		Log.d("DBug", this.getClass() +": onNewIntent()");
		setIntent(pIntent);
		
		String requestedPicture = pIntent.getExtras().getString("reqestedPicture");
		if ( requestedPicture != null ) {
			int picId = searchForPicture(requestedPicture);
			if ( picId != -1 ) {
				showPicture( picId );
			}
		} else {
			int picId = searchForPicture(pIntent.getExtras().getString(SearchManager.QUERY));
			if ( picId != -1 ) {
				showPicture( picId );
			}
		}
		
		super.onNewIntent(pIntent);
	}
	
	private int searchForPicture(String pSubstring) {
		
		for ( int i = 0; i < sPictureCount; ++i ) {
			if ( sPictures[i].compareToIgnoreCase(pSubstring) == 0 ) {
				return i;
			}
		}
		return -1;
	}
	
	/*************************************************************************/
	/* Menu-Methoden                                                         */
	/*************************************************************************/
	@Override
	public boolean onCreateOptionsMenu(Menu pMenu) {
		Log.d("DBug","ComixedViewer.java -> onCreateOptionsMenu()");
	    MenuInflater inflater = getMenuInflater();
	    inflater.inflate(R.menu.comixedviewer, pMenu);
	    Log.d("DBug",this.getClass() +" OptionsMenu inflated");
	    return true;
	}
	
	@Override
	public boolean onPrepareOptionsMenu(Menu pMenu) {
		Log.d("DBug","ComixedViewer.java -> onPrepareOptionsMenu()");
		
		pMenu.findItem(R.id.menuItemInfo).setEnabled(sPictureCount > 0);
		pMenu.findItem(R.id.menuItemJumpto).setEnabled(sPictureCount > 0);
		pMenu.findItem(R.id.menuItemSearch).setEnabled(sPictureCount > 0);
		Log.d("DBug","ComixedViewer.java -> OptionsMenu prepared");
		return super.onPrepareOptionsMenu(pMenu);
	}
	
	@Override
	public boolean onOptionsItemSelected(MenuItem pItem) {
		Log.d("DBug","ComixedViewer.java -> onOptionsItemSelected()");
		switch (pItem.getItemId()) {
			case R.id.menuItemInfo:
				mFileInformationDialog.show();
				break;
			case R.id.menuItemJumpto:
				mJumpToDialog.show();
				break;
			case R.id.menuItemSearch:
				onSearchRequested();
				break;
			case R.id.menuItemQuit:
				finish();
				break;
			case R.id.menuItemPreferences:
				Log.d("DBug","ComixedViewer.java -> erstelle Preferences-Intent...");
				Intent preferencesIntent = new Intent(this, ComixedViewerPreferences.class);
				Log.d("DBug","ComixedViewer.java -> Intent erstellt, starte Activity");
				startActivity(preferencesIntent);
				Log.d("DBug","ComixedViewer.java -> Intent gestartet!");
				break;
		}
		return true;
	}

	
}