package com.mhenlo.comixedviewer.search;

import android.app.Activity;
import android.app.SearchManager;
import android.content.Context;
import android.content.Intent;
import android.database.CharArrayBuffer;
import android.database.Cursor;
import android.graphics.drawable.BitmapDrawable;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.GridView;
import android.widget.LinearLayout;
import android.widget.SimpleCursorAdapter;
import android.widget.TextView;

import com.mhenlo.comixedviewer.ComixedViewer;
import com.mhenlo.comixedviewer.R;
import com.mhenlo.comixedviewer.provider.SearchSuggestions;
import com.mhenlo.comixedviewer.provider.SearchSuggestionsDatabase;

public class ComixedViewerSearch extends Activity {
	
	private GridView mGridView;
	private SearchSuggestionsDatabase mSearchSuggestionsDatabase;
	private LayoutInflater mLayoutInflater;
	private Cursor mCursor;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		Log.d("DBug", this.getClass() +": onCreate()");
		super.onCreate(savedInstanceState);
		
		
		
		// Get the intent, verify the action and get the query
		Intent intent = getIntent();
		if (Intent.ACTION_SEARCH.equals(intent.getAction())) {
			setContentView(R.layout.comixedviewersearch);
			String query = intent.getStringExtra(SearchManager.QUERY);
			String user_query = intent.getStringExtra(SearchManager.USER_QUERY);
			Log.d("DBug", "query: "+query);
			Log.d("DBug", "user_query: "+user_query);
			
			mLayoutInflater = (LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE);
			mSearchSuggestionsDatabase = new SearchSuggestionsDatabase(this);
			
			mCursor = mSearchSuggestionsDatabase.query(null, SearchSuggestions.KEY_WORD + " MATCH ?", new String[] {query.toLowerCase()+"*"}, null);
			
			mGridView = (GridView) findViewById(R.id.search_GridView_root);
			mGridView.setAdapter(new ImageAdapter(this, 0, mCursor, new String[0], new int[0]));
			mGridView.setOnItemClickListener(new OnItemClickListener() {
				public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
					Log.d("DBug", this.getClass() +": Element " +position +" ausgewaehlt");
//					Toast.makeText(ComixedViewerSearch.this, "" + position, Toast.LENGTH_SHORT).show();
					mCursor.moveToPosition(position);
					String pictureName = mCursor.getString( mCursor.getColumnIndex(SearchSuggestions.KEY_WORD) );
					
					Intent displayPicture = new Intent(ComixedViewerSearch.this, ComixedViewer.class);
					displayPicture.putExtra("reqestedPicture", pictureName);
					displayPicture.setFlags(Intent.FLAG_ACTIVITY_REORDER_TO_FRONT);
					startActivity(displayPicture);
//					finish();
					
				}
			});
		} else if (Intent.ACTION_VIEW.equals(intent.getAction())) {
			Intent displayPicture = new Intent(ComixedViewerSearch.this, ComixedViewer.class);
			displayPicture.putExtras(intent.getExtras());
			displayPicture.setFlags(Intent.FLAG_ACTIVITY_REORDER_TO_FRONT);
			startActivity(displayPicture);
//			finish();
		}
	}

	@Override
	protected void onPause() {
		finish(); // nach Moeglichkeit wieder entfernen, damit man aus dem Hauptbildschirm zurueck in das Auswahlmenue (der Suche) kommt
		super.onPause();
	}
	
	
	@Override
	protected void onDestroy() {
		Log.d("DBug", this.getClass() +": onDestroy()");
		if ( mSearchSuggestionsDatabase != null ) {
			mSearchSuggestionsDatabase.close(); // Datenbank zur Speicherfreigabe schliessen
		}
		super.onDestroy();
	}
	
	private class ImageAdapter extends SimpleCursorAdapter {
		
//		private Context mContext;
//		private int mLayoutId;
		private Cursor mCursor;
//		private String[] mFrom;
//		private int[] mTo;
		
		public ImageAdapter(Context pContext, int pLayoutId, Cursor pCursor, String[] pFrom, int[] pTo) {
			super(pContext, pLayoutId, pCursor, pFrom, pTo);
//			mContext = pContext;
//			mLayoutId = pLayoutId;
			mCursor = pCursor;
//			mFrom = pFrom;
//			mTo = pTo;
		}

		@Override
		public View getView(int pPosition, View pConvertView, ViewGroup pParent) {
			ViewCache viewCache;
			mCursor.moveToPosition(pPosition);
			
			if ( pConvertView == null ) {
				
				pConvertView = mLayoutInflater.inflate(R.layout.imageadapter_griditem, pParent, false);
				viewCache = new ViewCache();
				viewCache.mTextView = (TextView) pConvertView.findViewById(R.id.gridItem_TextView_thumb);
				
//				viewCache.mImageView = (ImageView) pConvertView.findViewById(R.id.gridItem_ImageView_thumb);
				viewCache.mImageView = (LinearLayout) pConvertView.findViewById(R.id.gridItem_ImageView_thumb);
				pConvertView.setTag(viewCache);
				
			} else {
				viewCache = (ViewCache) pConvertView.getTag();
			}
			
			// set content for TextView
			mCursor.copyStringToBuffer(mCursor.getColumnIndex(SearchSuggestions.KEY_WORD), viewCache.mTextViewBuffer);
			int size = viewCache.mTextViewBuffer.sizeCopied;
			viewCache.mTextView.setText(viewCache.mTextViewBuffer.data, 0, size);
			
			//set content for ImageView
//			viewCache.mImageView.setBackgroundResource(R.drawable.icon);
			
			String path = ComixedViewer.getEntirePath() +"/" +mCursor.getString( mCursor.getColumnIndex(SearchSuggestions.KEY_WORD) );
			Log.d("DBug", this.getClass() +": BitmapPfad: " +path);
			viewCache.mImageView.setBackgroundDrawable( new BitmapDrawable(path) );
			
			
			return pConvertView;
		}
	}
	
	final static class ViewCache {
//		public ImageView mImageView;
		public LinearLayout mImageView;
		public TextView mTextView;
		public CharArrayBuffer mTextViewBuffer = new CharArrayBuffer(128);
	}
}
