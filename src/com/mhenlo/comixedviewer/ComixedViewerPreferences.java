package com.mhenlo.comixedviewer;

import android.app.Activity;
import android.os.Bundle;
import android.preference.PreferenceActivity;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.widget.Toast;

public class ComixedViewerPreferences extends PreferenceActivity {
	
	@SuppressWarnings("unused")
	private Prefs mPrefs;
	
	
	protected void onCreate(Bundle savedInstanceState) {
		Log.d("DBug",this.getClass() +" OnCreate()");
		super.onCreate(savedInstanceState);
		addPreferencesFromResource(R.layout.preferences);
		
		mPrefs = new Prefs(this.getApplicationContext());
		Log.d("DBug",this.getClass() +" created");
	}
	
	@Override
	protected void onPause() {
		setResult(Activity.RESULT_OK);
		finish();
		super.onPause();
	}
	
	@Override
	public boolean onSearchRequested() {
		return false;
	}
	
	/*************************************************************************/
	/* Menu-Methoden                                                         */
	/*************************************************************************/
	@Override
	public boolean onCreateOptionsMenu(Menu pMenu) {
		MenuInflater inflater = getMenuInflater();
	    inflater.inflate(R.menu.preferences, pMenu);
		return super.onCreateOptionsMenu(pMenu);
	}
	
	@Override
	public boolean onOptionsItemSelected(MenuItem pItem) {
		switch (pItem.getItemId()) {
        case R.id.preferencesMenuItemSave:
        	Toast.makeText(this, ComixedViewerPreferences.this.getString(R.string.preferences_menu_saved), Toast.LENGTH_SHORT).show();	
        	break;
        case R.id.preferencesMenuItemBack:
        	this.setResult(Activity.RESULT_OK);
        	this.finish();
            break;
		}
		return super.onOptionsItemSelected(pItem);
	}
}
